##Ubuntu

###Docker

####Problem
Docker sometimes won't run

####Solution

    app-get install apparmor lxc cgroup-lite
    service start docker

See stackoverflow post [here](http://stackoverflow.com/a/29754871)

####Explanation
Install missing dependencies and start the docker daemon/service
Note sure if installing all dependencies are necessary
