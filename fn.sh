#!/usr/bin/env bash

#Delete All containers
dac(){
    local dockerPs=$(docker ps -aq)
    if [ ! -z "$dockerPs" ]; then
        docker rm -f $(docker ps -aq)
        echo "All containers removed"
    else
        echo "No containers"
    fi
}

#Print container IPs
pip(){
    if [ -z "$1" ];then
        echo "Need container id, run 'docker ps'"
        return
    fi
    if [ "$1" == "all" ];then
        dif $(docker ps -q)
        return
    fi
    dif "$@"
}

#Docker Inspect Format
#Print containers image name and ip
dif(){
    docker inspect -f "{{ .Config.Image }} {{ .NetworkSettings.IPAddress }}" "$@"
}

run(){
    local postgresId=$(rp)
    local jiraId=$(docker run -d -p 8080:8080 --link "$postgresId" durdn/atlassian-jira)
    pip "$postgresId"
    docker exec -i -t "$jiraId" bash
}

#Run Postgres
rp(){
    local hostRootDir=/opt/docker/postgresql
    local hostPostgresqlDataDir="$hostRootDir"/data
    local remoteDataDir=/var/lib/postgresql/data
    docker run -d -v "$hostPostgresqlDataDir":"$remoteDataDir" postgres
}

#Attach Bash
ab(){
    #May need error checking
    docker exec -i -t "$@" bash
}

#Build My Postgres instance and Run
bmpr(){
    imageName=mypostgres
    containerName=mypostgresContainer
    dataDir=/opt/docker/postgresql

    #Clean up
    docker rm -f "$containerName"
    docker rmi -f "$imageName"
    rm -rf "$dataDir"

    #Build and run
    docker build -t "$imageName" postgres
    docker run -d -e DATA_DIR="$dataDir" --name "$containerName" "$imageName"
}

# call arguments verbatim:
"$@"